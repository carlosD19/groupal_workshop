<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Response;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data'=>Product::all()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $category = Category::find($request->get('category_id'));
        if (!$category) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'No se encuentra una categoria con ese id.'])], 404);
        }
        $product = Product::create($request->all());
        $response = Response::make(json_encode(['data'=>$product ]), 201)->header('Location', 'http://localhost/api/product/'.$product ->id)->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product_id
     * @return \Illuminate\Http\Response
     */
    public function show($product_id)
    {
        $product = Product::find($product_id);
        if (!$product) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'No se encuentra una categoria con ese id.'])], 404);
        }
        return response()->json(['status'=>'ok', 'data'=>$product], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product_id)
    {
        $product = Product::find($product_id);
        if (!$product) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'No se encuentra un producto con ese id.'])], 404);
        }
        $name = $request->get('name');
        $sku = $request->get('sku');
        $description = $request->get('description');
        $price = $request->get('price');
        $stock = $request->get('stock');
        $category_id = $request->get('category_id');
        if ($category_id) {
            $category = Category::find($category_id);
            if (!$category) {
                return response()->json(['errors'=>array(['code'=> 404, 'message'=>'No se encuentra una categoria con ese id.'])], 404);
            }
        }
        if ($request->method() === 'PATCH') {
            $bus = false;
            if ($name) {
                $product->name = $name;
                $bus = true;
            }
            if ($sku) {
                $product->sku = $sku;
                $bus = true;
            }
            if ($description) {
                $product->description = $description;
                $bus = true;
            }
            if ($price) {
                $product->price = $price;
                $bus = true;
            }
            if ($stock) {
                $product->stock = $stock;
                $bus = true;
            }
            if ($category_id) {
                $product->category_id = $category_id;
                $bus = true;
            }
            if ($bus) {
                $product->save();
                return response()->json(['status'=>'ok','data'=>$product], 200);
            } else {
                return response()->json(['errors'=>array(['code'=>304, 'message'=>'No se ha modificado ningún dato del producto.'])], 304);
            }
        }
        if (!$name || !$sku || !$description || !$price || !$stock || !$category_id) {
            return response()->json(['errors'=>array(['code'=>422, 'message'=>'Faltan valores para completar el procesamiento.'])], 422);
        }
        $product->fill($request->all())->save();
        $product = Product::find($product_id);
        return response()->json(['status'=>'ok','data'=>$product], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($product_id)
    {
        $product = Product::find($product_id);
        if (!$product) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'No se encuentra un producto con ese id.'])], 404);
        }
        $product->delete();
        return response()->json(['status'=>'ok', 'data'=>$product], 200);
    }
}
