<?php

namespace App\Http\Controllers;

use App\Category;
use Response;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data'=>Category::all()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $category = Category::create($request->all());
        $response = Response::make(json_encode(['data'=>$category ]), 201)->header('Location', 'http://localhost/api/category/'.$category ->id)->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category_id
     * @return \Illuminate\Http\Response
     */
    public function show($category_id)
    {
        $category = Category::find($category_id);
        if (!$category) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'No se encuentra una categoria con ese id.'])], 404);
        }
        return response()->json(['status'=>'ok', 'data'=>$category], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category_id)
    {
        $category = Category::find($category_id);
        if (!$category) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'No se encuentra una categoria con ese id.'])], 404);
        }
        $name        = $request->get('name');
        $description = $request->get('description');
        if ($request->method() === 'PATCH') {
            $bus = false;
            if ($name) {
                $category->name = $name;
                $bus = true;
            }
            if ($description) {
                $category->description = $description;
                $bus = true;
            }
            if ($bus) {
                $category->save();
                return response()->json(['status'=>'ok','data'=>$category], 200);
            } else {
                return response()->json(['errors'=>array(['code'=>304, 'message'=>'No se ha modificado ningún dato de la categoria.'])], 304);
            }
        }
        if (!$name || !$description) {
            return response()->json(['errors'=>array(['code'=>422, 'message'=>'Faltan valores para completar el procesamiento.'])], 422);
        }
        $category->fill($request->all())->save();
        $category = Category::find($category_id);
        return response()->json(['status'=>'ok','data'=>$category], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($category_id)
    {
        $category = Category::find($category_id);
        if (!$category) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'No se encuentra una categoria con ese id.'])], 404);
        }
        $category->delete();
        return response()->json(['status'=>'ok', 'data'=>$category], 200);
    }
}
